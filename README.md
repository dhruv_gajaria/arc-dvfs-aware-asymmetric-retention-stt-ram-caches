# ARC DVFS-Aware Asymmetric-Retention STT-RAM Caches

Reference:
ARC: DVFS-Aware Asymmetric-Retention STT-RAM Caches for Energy-Efficient Multicore Processors
Dhruv Gajaria and Tosiron Adegbija
International Symposium on Memory Systems (MEMSYS), Washington DC, USA, October 2019

The folder "data" has the deadline based data used in simlations.
The folder "pre-emption" has the data used for more realistic assumption of preemption. We use multi-label classification for this problem.
The folder "code" contains the essential python code used for simulations. 

Please read README files in each of the folders.