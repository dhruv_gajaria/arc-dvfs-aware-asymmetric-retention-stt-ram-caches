======
README
======
We run run experiments in test_train_split2.py to find the best classifier.
The features for deciscion tree were empirically chosen based on the experiments. Each run gives in different accuracy levels.
We chose features for decision tree which results in highest accuracy.
The structure for the decision tree can be obtained in the code test_train_split2.py

======
Timing
======
For prediction timing we use timestamps before and after the prediction. We normalize the obtained time for 1 prediction

==============
Memory Profile
==============
We use mem_profile.py script to find the memory consumption for the machine learning classifier.

===============================================
Multi-label classification for pre-emption case
===============================================
Script multi-test_train.py represents our code for multi-label classification 