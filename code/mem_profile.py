# -*- coding: utf-8 -*-
"""
@author: dhruv
"""
#code to get the memory usage for the machine learning classification
from memory_profiler import profile
import pickle
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn import tree
import numpy as np
from sklearn.model_selection import train_test_split

data = np.genfromtxt('fb_results_multi_3m_b1_train_energy.csv', delimiter=',')#put training  data
X_train = data[1:,5:-4]#selection of rows and columns from the csv file into the array
y_train = data[1:,-4:]#training label data
data_test=np.genfromtxt('fb_results_multi_3m_b3_test_energy.csv',delimiter=',')#put testing data
X_test= data_test[1:,5:-4]
y_test=data_test[1:,-4:]#testing label data
precision = 10

@profile(precision=precision)
def ml_funct():
    gauss = tree.DecisionTreeClassifier()
    gauss.fit(X_train, y_train)
    s=pickle.dumps(gauss)
    clf2=pickle.loads(s)
    clf2.predict(X_test)
    #return mean

if __name__ == '__main__':
    ml_funct()

