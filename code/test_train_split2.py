# -*- coding: utf-8 -*-
"""
@author: dhruvgajaria
"""

import time
import numpy as np
import pandas as pd
#from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.svm import LinearSVC

data = np.genfromtxt('fb_results_3m_b1_train_energy.csv', delimiter=',')
X_train = data[1:,5:-1]
svc_train=0
svc_test=0
svc_time=0
gauss_time=0
gauss_train=0
gauss_test=0
bern_time=0
bern_train=0
bern_test=0
dectree_test=0
dectree_time=0
dectree_train=0
forest_test=0
forest_time=0
forest_train=0
rand_test=0
rand_time=0
rand_train=0
knn_test=0
knn_time=0
knn_train=0
#learning_Rate = 0.0212
y_train = data[1:,-1]
data_test=np.genfromtxt('fb_results_3m_b2_test_energy.csv',delimiter=',')
X_test= data_test[1:,5:-1]
y_test=data_test[1:,-1]

lin = svm.SVC(kernel='linear')
clf=LinearSVC(multi_class='ovr')
clf.fit(X_train, y_train)
a=time.clock()
clf.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
svc_time=svc_time+q
svc_train=(1 - clf.fit(X_train, y_train).score(X_train, y_train))+svc_train
svc_test=(1 - clf.fit(X_train, y_train).score(X_test, y_test))+svc_test
#label_predict_svc=[]
ynew_svc = clf.predict(X_test)
#label_predict_svc.append(ynew)

#print ("Testing time for individual sample linear SVC(ovr) is:",q)
#print ("training error for linear SVC(ovr) is:",1 - clf.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for linear SVC(ovr) is :",1 - clf.fit(X_train, y_train).score(X_test, y_test))

from sklearn.naive_bayes import GaussianNB
clf1 = GaussianNB()
clf1.fit(X_train, y_train)
a=time.clock()
clf1.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
gauss_time=gauss_time+q
gauss_train=gauss_train+(1 - clf1.fit(X_train, y_train).score(X_train, y_train))
gauss_test=(1 - clf1.fit(X_train, y_train).score(X_test, y_test))+gauss_test
ynew_gauss = clf1.predict(X_test)
#print ("Testing time for individual sampl for Gauss_nb is:",q)
#print ("training error for Gauss_nb  is:",1 - clf1.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for Gauss_nb is :",1 - clf1.fit(X_train, y_train).score(X_test, y_test))


from sklearn.naive_bayes import BernoulliNB
clf2 = BernoulliNB()
#clf.fit(X, Y)
clf2.fit(X_train, y_train)
a=time.clock()
clf2.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
bern_time=bern_time+q
bern_train=bern_train+(1 - clf2.fit(X_train, y_train).score(X_train, y_train))
bern_test=bern_test+(1 - clf2.fit(X_train, y_train).score(X_test, y_test))
ynew_bernoulli = clf2.predict(X_test)
#print ("Testing time for individual sample Bernoulli_NB is:",q)
#print ("training error for Bernoulli_NB  is:",1 - clf2.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for Bernoulli_NB is :",1 - clf2.fit(X_train, y_train).score(X_test, y_test))

from sklearn import tree
clf3= tree.DecisionTreeClassifier()
clf3.fit(X_train, y_train)
a=time.clock()
clf3.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
dectree_time=dectree_time+q
dectree_train=dectree_train+(1 - clf3.fit(X_train, y_train).score(X_train, y_train))
dectree_test=dectree_test+(1 - clf3.fit(X_train, y_train).score(X_test, y_test))
ynew_decision = clf3.predict(X_test)
foxtrot=clf3.feature_importances_
print(clf3.feature_importances_)
print(clf3.tree_.max_depth)
print(clf3.tree_.max_n_classes)
print(clf3.tree_.node_count)
n_nodes = clf3.tree_.node_count
children_left = clf3.tree_.children_left
children_right = clf3.tree_.children_right
feature = clf3.tree_.feature
threshold = clf3.tree_.threshold


# The tree structure can be traversed to compute various properties such
# as the depth of each node and whether or not it is a leaf.
node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
is_leaves = np.zeros(shape=n_nodes, dtype=bool)
stack = [(0, -1)]  # seed is the root node id and its parent depth
while len(stack) > 0:
    node_id, parent_depth = stack.pop()
    node_depth[node_id] = parent_depth + 1

    # If we have a test node
    if (children_left[node_id] != children_right[node_id]):
        stack.append((children_left[node_id], parent_depth + 1))
        stack.append((children_right[node_id], parent_depth + 1))
    else:
        is_leaves[node_id] = True

print("The binary tree structure has %s nodes and has "
      "the following tree structure:"
      % n_nodes)
for i in range(n_nodes):
    if is_leaves[i]:
        print("%snode=%s leaf node." % (node_depth[i] * "\t", i))
    else:
        print("%snode=%s test node: go to node %s if X[:, %s] <= %s else to "
              "node %s."
              % (node_depth[i] * "\t",
                 i,
                 children_left[i],
                 feature[i],
                 threshold[i],
                 children_right[i],
                 ))
print()

# First let's retrieve the decision path of each sample. The decision_path
# method allows to retrieve the node indicator functions. A non zero element of
# indicator matrix at the position (i, j) indicates that the sample i goes
# through the node j.

node_indicator = clf3.decision_path(X_test)

# Similarly, we can also have the leaves ids reached by each sample.

leave_id = clf3.apply(X_test)

# Now, it's possible to get the tests that were used to predict a sample or
# a group of samples. First, let's make it for the sample.

sample_id = 0
node_index = node_indicator.indices[node_indicator.indptr[sample_id]:
                                    node_indicator.indptr[sample_id + 1]]

print('Rules used to predict sample %s: ' % sample_id)
for node_id in node_index:
    if leave_id[sample_id] == node_id:
        continue

    if (X_test[sample_id, feature[node_id]] <= threshold[node_id]):
        threshold_sign = "<="
    else:
        threshold_sign = ">"

    print("decision id node %s : (X_test[%s, %s] (= %s) %s %s)"
          % (node_id,
             sample_id,
             feature[node_id],
             X_test[sample_id, feature[node_id]],
             threshold_sign,
             threshold[node_id]))

# For a group of samples, we have the following common node.
sample_ids = [0, 1]
common_nodes = (node_indicator.toarray()[sample_ids].sum(axis=0) ==
                len(sample_ids))

common_node_id = np.arange(n_nodes)[common_nodes]

print("\nThe following samples %s share the node %s in the tree"
      % (sample_ids, common_node_id))
print("It is %s %% of all nodes." % (100 * len(common_node_id) / n_nodes,))
#print(clf3.get_n_leaves)

#print ("Testing time for individual sample for for decision tree is:",q)
#print ("training error for decision tree  is:",1 - clf3.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for for decision tree is :",1 - clf3.fit(X_train, y_train).score(X_test, y_test))

from sklearn.ensemble import ExtraTreesClassifier
forest = ExtraTreesClassifier(n_estimators=200,random_state=0,class_weight='balanced_subsample')

forest.fit(X_train, y_train)
a=time.clock()
forest.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
forest_time=forest_time+q
forest_train=forest_train+(1 - forest.fit(X_train, y_train).score(X_train, y_train))
forest_test=forest_test+(1 - forest.fit(X_train, y_train).score(X_test, y_test))
ynew_ensemble = forest.predict(X_test)
#print ("Testing time for individual sample for ensemble is:",q)
#print ("training error for ensemble  is:",1 - forest.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for ensemble is :",1 - forest.fit(X_train, y_train).score(X_test, y_test))

from sklearn.ensemble import RandomForestClassifier
forest_1 = RandomForestClassifier(max_depth=5, random_state=0)
forest_1.fit(X_train, y_train)
a=time.clock()
forest_1.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
rand_time=rand_time+q
rand_train=rand_train+(1 - forest_1.fit(X_train, y_train).score(X_train, y_train))
rand_test=rand_test+(1 - forest_1.fit(X_train, y_train).score(X_test, y_test))
ynew_random_forest = forest_1.predict(X_test)
#print ("Testing time for individual sample for random forest is:",q)
#print ("training error for random forest  is:",1 - forest_1.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for random forest is :",1 - forest_1.fit(X_train, y_train).score(X_test, y_test))

from sklearn.neighbors import KNeighborsClassifier
#clf4 = KNeighborsClassifier(n_neighbors=17,weights='distance')
clf4=KNeighborsClassifier(n_neighbors=3)
clf4.fit(X_train, y_train)
a=time.clock()
clf4.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
knn_time=knn_time+q
knn_train=knn_train+(1 - clf4.fit(X_train, y_train).score(X_train, y_train))
knn_test=knn_test+(1 - clf4.fit(X_train, y_train).score(X_test, y_test))
ynew_knn = clf4.predict(X_test)
features_knn=clf4.kneighbors(X_test)
#features_knn.toarray()

time_list=[]
test_list=[]
train_list=[]

myint=1
time_list.extend((svc_time,gauss_time,bern_time,dectree_time,forest_time,rand_time,knn_time))
final_time=[x/myint for x in time_list]
test_list.extend((svc_test,gauss_test,bern_test,dectree_test,forest_test,rand_test,knn_test))
final_test=[x/myint for x in test_list]
train_list.extend((svc_train,gauss_train,bern_train,dectree_train,forest_train,rand_train,knn_train))
final_train=[x/myint for x in train_list]
timedata_stats=pd.DataFrame(final_time,index=["svc","gauss","Bernoulli","decision_tree","ensemble","random_forest","knn"],columns=["testing_time"])
traindata_stats=pd.DataFrame(final_train,index=["svc","gauss","Bernoulli","decision_tree","ensemble","random_forest","knn"],columns=["train_error"])
testdata_stats=pd.DataFrame(final_test,index=["svc","gauss","Bernoulli","decision_tree","ensemble","random_forest","knn"],columns=["test_error"])

result = pd.concat([timedata_stats,traindata_stats,testdata_stats], axis=1)
print(result)

