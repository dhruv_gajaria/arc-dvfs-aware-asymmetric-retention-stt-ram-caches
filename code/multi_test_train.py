# -*- coding: utf-8 -*-
"""
@author: dhruvgajaria
"""


import time
import numpy 
import pandas as pd

from sklearn.multioutput import MultiOutputClassifier

data = np.genfromtxt('fb_results_multi_3m_b1_train_energy.csv', delimiter=',')
X_train = data[1:,5:-4]
y_train = data[1:,-4:]
data_test=np.genfromtxt('fb_results_multi_3m_b4_test_energy.csv',delimiter=',')
X_test= data_test[1:,5:-4]
y_test=data_test[1:,-4:]


from sklearn import tree
clf3= tree.DecisionTreeClassifier()
multi_target_tree=MultiOutputClassifier(clf3)
a=multi_target_tree.fit(X_train,y_train).predict(X_test)
print(clf3.get_depth)
print(a)
